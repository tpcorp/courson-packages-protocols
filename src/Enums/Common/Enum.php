<?php

namespace Coursondev\Protocols\Enums\Common;

use BenSampo\Enum\Exceptions\InvalidEnumKeyException;
use BenSampo\Enum\Exceptions\InvalidEnumMemberException;
use Illuminate\Support\Str;

class Enum extends \BenSampo\Enum\Enum
{
    /**
     * Вызов метода.
     *
     * @param string $method
     * @param mixed $parameters
     * @throws InvalidEnumKeyException
     * @throws InvalidEnumMemberException
     * @return bool|mixed|null
     */
    public function __call($method, $parameters)
    {
        if (($result = $this->callIsMethod($method)) !== null) {
            return $result;
        }

        return parent::__call($method, $parameters);
    }

    /**
     * Создать объект по переданному ключу.
     *
     * @param string $key
     * @throws InvalidEnumKeyException
     * @throws InvalidEnumMemberException
     * @return static
     */
    public static function fromKey(string $key): self
    {
        if (static::hasKey($upperKey = Str::of($key)->snake()->upper())) {
            $enumValue = parent::getValue($upperKey);

            return new static($enumValue);
        }

        return parent::fromKey($key);
    }

    /**
     * Проверить на равенство переданного значения.
     *
     * @param $enumValue
     * @return bool
     */
    public function equals($enumValue): bool
    {
        return $this->is($enumValue);
    }

    /**
     * Метод сравнения по ключу.
     *
     * @param $method
     * @throws InvalidEnumKeyException
     * @throws InvalidEnumMemberException
     * @return bool
     */
    private function callIsMethod($method): ?bool
    {
        if (
            Str::contains($method, 'is')
            && $newMethod = Str::of($method)->after('is')->snake()->upper()
        ) {
            return static::fromKey($newMethod)->is($this);
        }

        return null;
    }
}
