<?php

namespace Coursondev\Protocols\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use Coursondev\Protocols\Enums\Common\Enum;

/**
 * @method static self Group
 * @method bool isGroup()
 *
 * @method static self Personal
 * @method bool isPersonal()
 *
 * @method static self PrintForm
 * @method bool isPrintForm()
 */
class Type extends Enum implements LocalizedEnum
{
    public const GROUP      = 'group';
    public const PERSONAL   = 'personal';
    public const PRINT_FORM = 'print_form';
}
